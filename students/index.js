const express = require("express");
const { connectTOMongoDb } = require('./connect');
const studentRoute = require('./routes/routes');
const cron = require("node-cron");
const shell = require("shelljs");
const cors = require("cors");
require('dotenv').config();
const PORT = process.env.PORT || 6000;
const app = express();
app.use(cors());
connectTOMongoDb('mongodb://127.0.0.1:27017/students').
    then(() => console.log("connected to mongodb"));

app.use(express.json());


app.use("/add", studentRoute);

cron.schedule("59 * * * *", () => {
    console.log("time hogya bhai")
})
// if (shell.exec("dir").code !== 0) {
//     console.log("something went wrong");
// }

app.listen(PORT, () => console.log("listening"));