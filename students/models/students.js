const mongoose = require('mongoose');
const Joi = require('joi');

//schema
const studentSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: false,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    dialCode: {
        type: String,
        required: true,
    },
    phoneNumber: {
        type: Number,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    collegeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "college",
    },
});
studentSchema.index({ phoneNumber: 1, dialCode: 1 }, { unique: true });

const marksSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Student",
    },
    subject: {
        type: String,
    },
    marks: {
        type: Number,
    }
});
const marks = mongoose.model('marks', marksSchema);
//collegeSchema
const collegeSchema = new mongoose.Schema({
    collegeName: {
        type: String,
    },
});
const college = mongoose.model('college', collegeSchema);
//JoiSchema
const studentJoiSchema = Joi.object({
    firstName: Joi.string().required(),
    lastName: Joi.string(),
    email: Joi.string().email().required(),
    dialCode: Joi.string().required().min(2).max(10),
    phoneNumber: Joi.number().required().min(10),
    password: Joi.string().required().min(4),
});
//JoiSchema
const loginJoiSchema = Joi.object({
    email: Joi.string().email(),
    dialCode: Joi.string().min(2).max(10),
    phoneNumber: Joi.number().min(10),
    password: Joi.string().required().min(4),
});
const students = mongoose.model('Student', studentSchema);

//marksSchema


const jtiSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    jti: {
        type: String,
        required: true,
    }
})
const jti = mongoose.model('jti', jtiSchema);

//photos Schema
// const imageSchema = new mongoose.Schema({
//     userId: {
//         type: String,
//         required: true,
//     },
//     imageUrl: {
//         type: String,
//     },
//     imageTitle: {
//         type: String,
//     },
//     imageDesc: {
//         type: String,
//     },
//     uploaded: {
//         type: Date,
//         default: Date.now,
//     }});
//     const image = mongoose.model('image',imageSchema);

module.exports = {
    students,
    studentJoiSchema,
    loginJoiSchema,
    marks,
    jti,
    // image,
    college,
}