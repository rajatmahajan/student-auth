const Student = require('../models/students');
const EventEmitter = require('events');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const nodeMailer = require('nodemailer');
const { default: mongoose } = require('mongoose');


require('dotenv').config();
const JWT_SECRET = process.env.secretKey;

const event = new EventEmitter();

//Counting API hits
let count = 0;
event.on("countApi", () => {
    count++
    console.log("event count", count);
})

//adding students as an array of objects
async function addStudent(req, res) {
    try {
        // await Student.studentJoiSchema.validateAsync(req.body);
        const body = req.body;
        const salt = await bcrypt.genSalt(10); // Await the salt generation
        const secPass = await bcrypt.hash(body.password, salt); // Await the hash generation
        if (await Student.students.findOne({ "email": body.email })) {
            return res.status(409).json({ message: "email already exists" });
        }
        else if (await Student.students.findOne({ "phoneNumber": body.phoneNumber })) {
            return res.status(409).json({ message: "Number is already registered" });
        }
        const obj = {
            firstName: body.firstName,
            lastName: body.lastName,
            email: body.email,
            dialCode: body.dialCode,
            phoneNumber: body.phoneNumber,
            password: secPass,
            college: body.collegeId,//new added
        };
        const student = await Student.students.create(obj);
        event.emit("countApi")
        console.log(student);
        return res.status(201).json({ msg: "successfull added new user" });
    }
    catch (error) {
        console.log(error);
        return res.status(400).json({ error: "invalid entry" });
    }
}
//showing name and lastname of students in db
async function AllstudentsDetails(req, res) {
    try {
        let queryObject = {};
        let page = Number(req.query.page) || 1;
        let limit = Number(req.query.limit) || 3;
        let query = req.query.subject || null;
        const startIndex = (page - 1) * limit;
        const mysort = { 'firstName': 1 };
        if (query != null) {
            queryObject = {
                $or: [
                    { firstName: { $regex: new RegExp(query, 'i') } },
                    { lastName: { $regex: new RegExp(query, 'i') } }
                ]
            };
        }
        const count = await Student.countDocuments(queryObject);
        const totalPages = Math.ceil(count / limit);
        let detailsQuery = Student.find(queryObject, { "password": 0, _id: 0 }).sort(mysort).populate("collegeId");
        detailsQuery = detailsQuery.skip(startIndex).limit(limit);
        const details = await detailsQuery;
        return res.json({
            data: details,
            count: totalPages,
        });
    }
    catch (error) {
        console.error(error);
        return res.status(500).json({ error: "Internal Server Error" });
    }
}
async function collegestudent(req, res) {
}

//finding the student according to the db id 
async function findstudent(req, res) {
    try {
        const body = req.body;
        const student = await Student.students.findByIdAndUpdate(req.params.id);
        if (!student) {
            return res.status(404).json({ error: "User not found" });
        }
        return res.json(student);
    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: "Internal server error" });
    }
}
//updating student information
async function updatestudent(req, res) {
    try {
        console.log(req.body);
        const { subject } = req.body;
        const updatedStudent = await Student.students.updateOne({ "id": req.params.id },
            { $set: subject, });
        if (!updatedStudent) {
            return res.status(404).json({ error: "Student not found" });
        }
        // Send the updated student document as the response
        return res.json(updatedStudent);
    }
    catch (error) {
        console.log(`there is an error - ${error}`);
    }
}
async function updateEntireStudent(req, res) {
    try {
        const id = req.params.id;
        const body = req.body;
        const updatedStudent = await Student.students.findOneAndUpdate({ _id: id }, {
            firstName: body.firstName,
            lastName: body.lastName,
            email: body.email,
            dialCode: body.dialCode,
            phoneNumber: body.phoneNumber
        });
        if (!updatedStudent) {
            return res.status(404).json({ error: "Student not found" });
        }
        return res.json(updatedStudent);

    }
    catch (err) {
        console.log(err);
    }
}
//deleting student
async function deletestudent(req, res) {
    try {
        const id = req.params.id;
        const deletedstudent = await Student.students.findByIdAndDelete(id)
        if (!deletedstudent) {
            return res.status(404).json({ error: "user doesnt exists" });
        }
        const deleted = await Student.marks.findByIdAndDelete({ "userId": id })
        return res.json({ deletedstudent, deleted })
    }
    catch (error) {
        console.log(`there is an error - ${error}`);
    }
}
//adding subject and marks to existing student
async function addMarks(req, res) {
    try {
        const body = req.body;
        const existing = await Student.marks.find({ "userId": body.userId, "subject": body.subject });
        if (existing.length > 0) {
            return res.status(400).json({ error: " already exists" });
        }
        const obj = new Student.marks(body);
        console.log(obj);
        const subjectMarksObj = await obj.save();
        const student = await Student.students.findByIdAndUpdate(body.userId);

        // .populate('marks').exec((err, student) => {
        //     if (err) {
        //         console.error(err);
        //         return;
        //     }
        if (!student) {
            return res.status(404).json({ error: "Student not found" });
        }
        return res.status(200).json({
            success: true,
            message: "Subject marks added for the student",
            data: student,
        });
    } catch (error) {
        console.error("Error:", error);
        return res.status(500).json({ error: "Server error" });
    }
}
//Authentication of the user , encrypting password
async function login(req, res) {
    const { email, password, phoneNumber, dialCode } = req.body;
    try {
        // console.log(req.body);

        await Student.loginJoiSchema.validate(req.body);
        let user;
        if (!password) {
            return res.status(400).json({ error: "Password is required" });
        }
        if (email && phoneNumber && dialCode) {
            user = await Student.students.findOne({ "email": email, "phoneNumber": phoneNumber, "dialCode": dialCode });
            // let user2 = await Student.students.findOne({ "phoneNumber": phoneNumber, "dialCode": dialCode });
            // if ((user1 === undefined && user2 !== undefined) || (user1 === undefined && user2 !== undefined)) {
            //     res.status(401).json("Either password or Number is wrong");
            // }
            // else if (user1 == undefined) {
            //     user = user2;
            // }
            // else {
            //     user = user1;
            // }
            if (user === null) {
                if (await Student.students.findOne({ "email": email, "phoneNumber": phoneNumber })) {
                    user = await Student.students.findOne({ "email": email, "phoneNumber": phoneNumber })
                }
                else if (await Student.students.findOne({ "email": email }) && await Student.students.findOne({ "phoneNumber": phoneNumber })) {
                    return res.status(409).json({ message: "Either the phone number or the email is incorrect" });
                }
                else {
                    user = null;
                }
            }
        }
        else if (email) { user = await Student.students.findOne({ "email": email }); }
        else if (phoneNumber && dialCode) {
            user = await Student.students.findOne({ "phoneNumber": phoneNumber, "dialCode": dialCode });
        }
        else if (phoneNumber) {
            console.log("hey")
            // dialCode = "+91";
            user = await Student.students.findOne({ "phoneNumber": phoneNumber, "dialCode": "+91" });
        }
        // && dialCode.equals("+91")
        else {
            return res.status(400).json({ error: "Invalid request" });
        }
        if (!user) {
            return res.status(400).json({ error: "user doesnt exists" });
        }
        const passwordCompare = await bcrypt.compare(password, user.password);
        if (!passwordCompare) {
            return res.status(400).json({ error: "wrong password" });
        }
        // if (passwordCompare) {
        //     console.log("welcome");
        // }
        // else {
        //     console.log("1");
        // }
        const data = {
            user: {
                id: user._id
            }
        }
        // randomNumber1 = Math.random();
        // let x = Math.floor((Math.random() * 10) + 1);
        // const jti = randomNumber1 + "-" + makeid(x) + "-" + (new
        //     Date().getTime() / 1000 | 0) + 5 * 60;
        // await Student.jti.findOneAndUpdate({ email: email }, { jti: jti });
        const authtoken = jwt.sign(data, JWT_SECRET, { expiresIn: "6hr" });
        return res.json({
            authtoken
        })
    }
    catch (error) {
        console.log(error);
        return res.status(401).json({ error: "U are not authorized" });
    }

}
function makeid(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        counter += 1;
    }
    return result;
}
//getting user from access token
async function getUserFromToken(req, res) {
    try {
        const token = req.header("authentication");
        if (!token) {
            res.status(401).send({ error: "invlid token" })
        }
        const data = jwt.verify(token, JWT_SECRET);
        req.user = data.user;
        userid = req.user.id;
        const user = await Student.students.findById(userid).select("-password")
        res.send(user);
        if (!user) {
            return res.status(400).json({ message: "Unable to access" });
        }
        return res.status(200).json({ message: "Successfully accessed" });
    }
    catch (err) {
        console.log("error", err);
        return res.status(500).json({ message: "server error" });
    }
}
//sending mail
async function sendmail(req, res) {
    try {
        const body = req.body;

        const transporter = nodeMailer.createTransport({
            host: "smtp.gmail.com",
            port: 465,
            secure: true,
            auth: {
                user: "rajatm020@gmail.com",
                pass: "osek ejlx vjsg pwzw",
            }
        });
        const info = await transporter.sendMail({
            from: '"Admin"<rajatm020@gmail.com>',
            to: "rajatm02020@gmail.com",
            subject: "hello",
            text: "hello sir",
            html: "<b>Hello world<b>"
        });
        console.log("message sent:%s", info.messageId);
        transporter.close();
        return res.status(200).send("Email sent successfully!");
    } catch (err) {
        console.error(err);
        res.status(500).send("Server error");
    }
}
//sending media multer
// async function sendMedia(req, res, next) {
//     try {
//     if(!req.file){
//         return res.status(400).send({message:"bad request"});
//     }
//         const userId=req.params.id;
//         const user=await Student.students.findById(userId);
//         if (!user) {
//             return res.status(404).send('User not found.');
//         }
//         user.image=req.file.filename;
//         await user.save();
//         return res.json({ body });
//     } catch (err) {
//         console.log(err);
//         return res.status(400).send("Error");
//     }}
async function addMarks(req, res) {
    try {
        const body = req.body;
        const existing = await Student.marks.find({ userId: body.userId, subject: body.subject });
        if (existing.length > 0) {
            return res.status(400).json({ error: "Mark already exists" });
        }
        const mark = new Student.marks(body);
        const savedMark = await mark.save();
        const updatedStudent = await Student.students.findByIdAndUpdate(body.userId, { $push: { marks: savedMark._id } }, { new: true }).populate('marks');
        if (!updatedStudent) {
            return res.status(404).json({ error: "Student not found" });
        }
        return res.status(200).json({
            success: true,
            message: "Subject marks added for the student",
            data: updatedStudent,
        });
    } catch (error) {
        console.error("Error:", error);
        return res.status(500).json({ error: "Server error" });
    }
}

//college
async function college(req, res) {
    try {
        const { student, collegeId } = req.body;
        const newCollege = new Student.college({ _id: collegeId });
        const savedCollege = await newCollege.save();
        const updatedStudent = await Student.findByIdAndUpdate(student, { collegeId: savedCollege._id }, { new: true }).populate('collegeId');
        res.status(200).json({ student: updatedStudent });
    } catch (error) {
        console.error("Error:", error);
        return res.status(500).json({ error: "Server error" });
    }
}
//showing marks
async function getMarks(req, res) {
    try {
        const body = req.body;
        const id = req.params.id;
        console.log(id);
        const marks = await Student.marks.find({ "userId": id });
        console.log(marks);
        return res.status('200').json({ marks });
    }
    catch (err) {
        console.log(err);
        return res.status('404').json({ message: "user not found" });
    }
}
//checking for empty
function checkFieldsNotEmpty(obj) {
    if (obj.firstName == null || obj.lastName == null || obj.email == null || obj.phoneNumber == null || obj.dialCode == null || obj.password == null) {
        return false;
    }
    else return true;
}
//sending mail when student is failed in 3 or more subjects
async function sendfailmail(req, res) {
    try {
        const body = req.body;
        const id = req.params.id;
        console.log(id);
        const marks = await Student.marks.find({ "userId": id });
        const student = await Student.students.findOne({ "_id": id });
        let count = 0, totalcount = 0;
        console.log(marks);
        console.log("b");
        console.log(student);
        marks.forEach(item => {
            totalcount++
            if (item.marks < 40) {
                count++;
            }
        });
        console.log(count);
        if (count >= 3 && checkFieldsNotEmpty(student)) {
            const transporter = nodeMailer.createTransport({
                host: "smtp.gmail.com",
                port: 465,
                secure: true,
                auth: {
                    user: "rajatm020@gmail.com",
                    pass: "osek ejlx vjsg pwzw",
                }
            });
            const info = await transporter.sendMail({
                from: '"Admin"<rajatm020@gmail.com>',
                to: student.email,
                subject: "Warning",
                text: `u are failing in ${count}`,
                html: "<b>You need to clear the backlogs<b>",
                replyTo: 'fullstacktester@yopmail.com',
            });
            console.log("message sent:%s", info.messageId);
            transporter.close();
            return res.status(200).json({ message: "Email sent successfully!" });
        } else {
            return res.status(404).json({ message: "Not enough data" });
        }
    } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
}
async function updateMarks(req, res) {
    try {
        const subject = req.query.subject;
        const id = req.params.id;
        const { marks } = req.body;
        const updated = await Student.marks.updateOne({ userId: id, subject: subject }, { $set: { marks: marks } });
        console.log(marks);
        if (updated.nModified === 0) {
            return res.status(404).json({ message: "user not found" });
        }
        return res.status(200).json({ message: "Updated Marks" });
    } catch (err) {
        return res.status(500).json({ message: "server error" });
    }
}
async function deleteMarks(req, res) {
    try {
        const id = req.params.id;
        console.log(id);
        const subject = req.query.subject;
        console.log(subject);
        const student = await Student.marks.findOneAndDelete({ 'userId': id, 'subject': subject })
        console.log(student);
        if (!student) {
            return res.status(404).json({ message: "user doesnt exists" });
        }
        return res.status(200).json({ message: "marks Deleted" });
    }
    catch (err) {
        console.log(err);
        return res.status(500).json({ message: "Server Error" });

    }
}
async function findOneSubject(req, res) {
    try {
        const id = req.params.id;
        const body = req.body;
        const sub = await Student.marks.findOne({ "userId": id, 'subject': body.subject });
        console.log(sub);
        if (!sub) {
            return res.status(404).json({ message: "Subject doesnt exists" });
        }
        return res.status(200).json(sub);
    }
    catch (err) {
        console.log(err);
        return res.status(500).json({ message: "Server Error" });
    }
}
async function AgendaImplement(req, res) {
    // const date = new Date(2024, 2, 7, 15, 0, 0);
    // const job = schedule.scheduleJob(date, function () {
    //     sendmail();
    //     console.log("mailed");
    // });
    try {
        await sendmail();
        console.log('mail sent');
    }
    catch (err) {
        console.error('error', err);
    }
    agenda.on('ready', () => {
        const date = new Date(2024, 2, 13, 5, 49, 0);
        agenda.schedule(date, 'send email');
        console.log("Job scheduled successfully.");
    })
}
async function seePhoto(req, res) {
}
async function aggregation(req, res) {
    try {
        const studentId = req.params.id;
        const aggregationResult = await Student.marks.aggregate([
            {
                $match: {
                    studentId: mongoose.Types.ObjectId(studentId),
                },
            },
        ]);
        return res.json({ aggregationResult })

    } catch (error) {
        console.error(error);
        return res.status(500).json({ message: "Internal server error" });
    }
}

module.exports = {
    addStudent,
    AllstudentsDetails,
    findstudent,
    updatestudent,
    deletestudent,
    addMarks,
    login,
    getUserFromToken,
    sendmail,
    // sendMedia,
    sendfailmail,
    getMarks,
    updateMarks,
    deleteMarks,
    findOneSubject,
    AgendaImplement,
    updateEntireStudent,
    college,
    collegestudent,
    aggregation,
}

// async function getUserFromToken(req, res) {

// try {
// const { UserToken } = req.body
// console.log("hello")
// const decodeToken = jwt.verify(UserToken, JWT_SECRET);
// const user = decodeToken.user;
// const student = await Student.students.findById(user);
// console.log(student);
// return res.json({ student });
// const head = req.headers('Authentication');
// if (typeof head !== undefined) {
//     const info = head.split(".");
//     const token = info[1];
//     req.token = token;
//     jwt.verify(req.token, JWT_SECRET, (err, authtoken) => {
//         if (err) {
//             res.send({ result: "invalid" })
//         } else {
//             res.json({
//                 msg: "profile accessed"
//             })
//         }
//     })
// } else {
//     res.send({
//         result: "token  is not valid"
//     })
// }

//     const { UserToken } = req.body;
//     console.log(UserToken);
//     const decodeToken = jwt.verify(UserToken, JWT_SECRET);
//     const userId = decodeToken.user;
//     if (!userId) {
//         throw new Error("User ID not found in token payload");
//     }
//     const student = await Student.findById(userId);
//     if (!student) {
//         throw new Error("Student not found with the provided user ID");
//     }
//     console.log(student);
//     return res.json({ student });
// }
// catch (error) {
//     console.log(`error ,${error}`);
//     return res.status(400).json({ error: "error in the token" })
// }

