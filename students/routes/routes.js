const express = require('express');
const { addStudent, AllstudentsDetails, findstudent, updatestudent, deletestudent, addMarks, login, getUserFromToken, sendmail, sendMedia, sendfailmail, getMarks, updateMarks, deleteMarks, findOneSubject, search, updateEntireStudent, college, collegestudent, aggregation } = require('../controllers/student');
const router = express.Router();
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'media')
    },
    filename: (req, file, cb) => {
        console.log(file)
        cb(null, file.originalname);
    }
});
const upload = multer({ storage: storage });

router.post('/', addStudent);
router.get('/showall', AllstudentsDetails);
router.get('/findone/:id', findstudent);
router.patch('/update/:id', updatestudent);
router.patch('/entireUpdate/:id', updateEntireStudent)
router.delete('/delete/:id', deletestudent);
router.post('/addmarks', addMarks);
router.post('/login', login);
router.get('/login/getinfo', getUserFromToken);
router.post('/sendMail', sendmail);
router.post('/sendMedia', upload.single("file"), (req, res, next) => {
    sendMedia(req, res, next);
});
router.post('/sendFailMail/:id', sendfailmail);
router.get('/getMarks/:id', getMarks);
router.patch('/updateMarks/:id', updateMarks);
router.delete('/deleteMarks/:id', deleteMarks);
router.get('/findOneSubject/:id', findOneSubject);
router.post('/addCollege', college);
router.post('/addstudentcollege', collegestudent);
router.get('/aggregation/:id', aggregation);
module.exports = router;    